#!/usr/env/bin python
# http://www.pythonchallenge.com/pc/return/good.html

# connect the dots ...
import re, urllib
from PIL import Image, ImageDraw
url = 'http://huge:file@www.pythonchallenge.com/pc/return/good.html'
ht = urllib.urlopen(url).read().split('\n\n')[6:8]
cp = [[int(p) for p in re.findall('(?::)?(\d+)', t)] for t in ht]
xy1,xy2 = [zip(c[0::2], c[1::2]) for c in cp]
im = Image.new('RGB',(640,480))
dr = ImageDraw.Draw(im)
dr.polygon(xy1, outline=(0,255,0))
dr.polygon(xy1, outline=(255,0,0))
del dr
im.save('good.jpg')
im.show()				# it's a cow... no, a bull :)