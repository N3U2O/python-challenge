#!/usr/bin/env python
# http://www.pythonchallenge.com/pc/def/ocr.html
with open('ch02.htm','rU') as f: mess=f.read()
s,chars,rates = '','',[]  # sol., characters and their rates
i1 = mess.find('%')
i2 = mess.rfind('*')
mess = ''.join(mess[i1:i2].split('\n'))
for c in mess:
    if c not in chars:
        chars += c
        rates += [1]
    else:
        rates[chars.find(c)] += 1
for i,r in enumerate(rates):
    if r < 10: s += chars[i]
print s