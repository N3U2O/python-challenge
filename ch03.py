#!/usr/bin/env python
# http://www.pythonchallenge.com/pc/def/equality.html
import re
with open('ch03.htm','rU') as f: t = f.read()
print ''.join(re.findall('[a-z][A-Z]{3}([a-z])[A-Z]{3}[a-z]', t))