#!/usr/bin/env python
# http://www.pythonchallenge.com/pc/def/channel.html

# download zip
# import urllib
# z = urllib.urlopen('http://www.pythonchallenge.com/pc/def/channel.zip').read()
# with open('channel.zip','wb') as zf: zf.write(z)

import re, zipfile
z = zipfile.ZipFile('channel.zip')			# open zipfile
n = re.findall('\d{3,5}',z.read('readme.txt'))[0]	# extract # from readme
s = ''							# output
while n:
    l = re.findall('\d{2,5}',z.read(n+'.txt'))		# find next n in n-th file
    s += z.getinfo(n+'.txt').comment			# collect the comment
    n = l[0] if l != [] else 0				# next n->n or 0->n if no next n found
print s							# print the output