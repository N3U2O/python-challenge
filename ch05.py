#!/usr/bin/env python
# http://www.pythonchallenge.com/pc/def/peak.html
import pickle, urllib
ps = urllib.urlopen('http://www.pythonchallenge.com/pc/def/banner.p').read()
po = pickle.loads(ps)
for row in po: print ''.join([n*c for c,n in row])
