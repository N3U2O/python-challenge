#!/usr/bin/env python
# http://www.pythonchallenge.com/pc/def/oxygen.html

import PIL.Image, re
p = PIL.Image.open('oxygen.png')
w,h = p.size
s = ''.join(chr(p.getpixel((7*x,h/2))[0]) for x in range(w/7))
print ''.join([chr(int(d)) for d in re.findall('\d{3}',s)])

